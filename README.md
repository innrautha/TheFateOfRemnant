# README.MD

## Introduction
*The FATE of Remnant* is a TTRPG based on the Fate system using the setting from
RWBY. If you just want to play use the PDF located in the `output` folder

## Files
* .build_scripts/ Scripts used to build the PDFs
* front/                Frontmatter for the PDF
* main/                 Mainmatter for the PDF
* back/                 Backmatter for the PDF
* appendix/             Appendices
* assets/               Images and Fonts used in constructing the PDF
* output/               Built PDFs
* TheFateOfRemnant.tex  The main tex file that contains the document structure
* TheFateOfRemnant.sty  The main sty file that contains all the other sty's and layout information
* makefile              Used to build the PDFs

## Dependences
Building the pdf requires

* make
* wget - to download fonts which I was unsure could be packaged with the project.
* XeLaTeX
* makeglossary
* All packages included in .tex and .sty files. List rapidly becomes out of date.

* Using `make` will cause 

## Make
To compile run

    make all

    make clean

If issues are encountered during the make process try

    make debug

and file a bug report with the output
