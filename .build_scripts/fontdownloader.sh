mkdir -p assets/fonts

if [ ! -f "assets/fonts/Fate Core Font.ttf" ];
then
  mkdir /tmp/fontdownloader
  wget -O /tmp/fontdownloader/fatefont.zip http://www.faterpg.com/wp-content/uploads/2013/06/Fate-Core-Font.ttf_.zip
  unzip "/tmp/fontdownloader/fatefont.zip" -d /tmp/fontdownloader
  mv "/tmp/fontdownloader/Fate Core Font.ttf" assets/fonts
  rm -r /tmp/fontdownloader
fi
