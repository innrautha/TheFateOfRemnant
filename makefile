#
# Makefile for Bachelorthesis
#
#   Andreas Linz -- 10INB-T
#   HTWK-Leipzig, Fakultaet IMN
#   klingt.net@googlemail.com
#
# @ supresses command output
#
# conditionals are not allowed to have a starting tab,
# otherwise they will be sent to the shell
#
PROJECTNAME=TheFateOfRemnant

DEBUG='true'
ROOT_DIR=$(shell pwd)
BUILD_DIR=$(ROOT_DIR)/.build
OUTPUT_DIR=$(ROOT_DIR)/output/

.PHONY: assets

all: assets copy_sources screen copy_output

debug: assets copy_sources screen_debug copy_output

assets:
	@echo "Obtaining Fonts ..."
	bash ./.build_scripts/fontdownloader.sh

copy_sources:
	@echo "Copying sources to build folder: $(BUILD_DIR)"
	@rsync --verbose --checksum --recursive --human-readable --progress --exclude=.build --exclude=.build_scripts --exclude=output --exclude=.git --exclude=.gitignore $(ROOT_DIR)/ $(ROOT_DIR)/.build

copy_output:
	@mkdir -p $(OUTPUT_DIR)
	@echo "Copying generated PDFs to output folder: $(OUTPUT_DIR)"
	@cp -f $(BUILD_DIR)/*.pdf $(OUTPUT_DIR)
	@echo "Output should be located at in $(OUTPUT_DIR)"

screen:
	@echo "Building screen version ..."
	@echo "0. Compiling Grimm Cards with XeLaTeX"
	@cd $(BUILD_DIR) && xelatex $(XELATEX_OPTS) GrimmCards > /dev/null
	@echo "0. Compiling Character Sheet with XeLaTeX"
	@cd $(BUILD_DIR) && xelatex $(XELATEX_OPTS) CharacterSheet > /dev/null
	@echo "1. First run of XeLaTeX"
	@cd $(BUILD_DIR) && xelatex $(XELATEX_OPTS) $(PROJECTNAME) > /dev/null
	@echo "2. Making Glossary"
	@cd $(BUILD_DIR) && makeglossaries $(PROJECTNAME) > /dev/null
	@echo "3. Second run of XeLaTeX"
	@cd $(BUILD_DIR) && xelatex $(XELATEX_OPTS) $(PROJECTNAME) > /dev/null

screen_debug: assets copy_sources
	@echo "Building screen version ..."
	@echo "12345: $(XELATEX_OPTS)"
	@echo "\n------------------ xelatex (0/3) --------------------\n"
	@cd $(BUILD_DIR) && xelatex $(XELATEX_OPTS) CharacterSheet
	@cd $(BUILD_DIR) && xelatex $(XELATEX_OPTS) GrimmCards
	@echo "\n------------------ xelatex (1/3) --------------------\n"
	@cd $(BUILD_DIR) && xelatex $(XELATEX_OPTS) $(PROJECTNAME)
	@echo "\n-------------- makeglossaries (2/3) -----------------\n"
	@cd $(BUILD_DIR) && makeglossaries $(PROJECTNAME)
	@echo "\n------------------ xelatex (3/3) --------------------\n"
	@cd $(BUILD_DIR) && xelatex $(XELATEX_OPTS) $(PROJECTNAME)

clean_conflicts:
	@find $(ROOT_DIR) -name '*in Konflikt stehende*' -delete
	@find $(ROOT_DIR) -name '*conflicted copy*' -delete

clean: clean_conflicts
	@echo "I will clean up this mess ..."
	@cd $(BUILD_DIR); rm -r *

clean_all: clean
	@rm $(OUTPUT_DIR)/*

clearscreen:
	clear
